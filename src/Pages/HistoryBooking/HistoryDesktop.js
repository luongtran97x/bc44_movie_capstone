import moment from "moment/moment";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { https } from "../../Services/config";
import "./History.css";
export default function HistoryDesktop() {
  const [userInfo, setUserInfo] = useState([]);
  

  useEffect(() => {
    https
      .post("api/QuanLyNguoiDung/ThongTinTaiKhoan")
      .then((res) => {
        setUserInfo(res.data.content);
      })
      .catch((err) => {
        console.log("🚀 ~ err:", err);
      });
  }, []);

  const renderHistory = () => {
    if (userInfo.length !== 0) {
      return userInfo.thongTinDatVe.map((item) => {
        return (
          <li
            key={item.maVe}
            className="historyBooking_table_row flex justify-between rounded font-bold "
            style={{
              background: "#bdbdbd",
              marginBottom: "25px",
              padding: "25px 30px",
            }}
          >
            <div className="historyBooking_col" style={{ width: "150px" }}>
              {item.maVe}{" "}
            </div>
            <div className="historyBooking_col" style={{ width: "150px" }}>
              {item.tenPhim}{" "}
            </div>
            <div className="historyBooking_col" style={{ width: "150px" }}>
              {moment(item.ngayDat).format("DD/MM/YYYY : hh:mm")}
            </div>
            <div className="historyBooking_col" style={{ width: "150px" }}>
              {item.danhSachGhe.map((dsGhe, index) => {
                return <span key={index}>{dsGhe.tenGhe} </span>;
              })}
            </div>
            <div className="historyBooking_col" style={{ width: "150px" }}>
              {item.danhSachGhe[0].tenHeThongRap}{" "}
            </div>
          </li>
        );
      });
    }
  };

  return (
    <div
      style={{
        background:
          "url(https://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)",
      }}
    >
      <div className="container">
        <p className="text-center text-5xl font-bold text-white py-5">History Booking</p>
        <ul className="historyBooking_responsive ">
          <li
            style={{ marginBottom: "25px", padding: "25px 30px" }}
            className="history_table_header flex justify-between rounded px-4 py-5 text-xl font-bold leading-3 bg-orange-600 text-black"
          >
            <div className="historyBooking_col">
              <p>ID TICKET</p>
            </div>
            <div className="historyBooking_col">
              <p>FLIM</p>
            </div>
            <div className="historyBooking_col">
              <p>DATE</p>
            </div>
            <div className="historyBooking_col">
              <p>SEAT</p>
            </div>
            <div className="historyBooking_col">
              <p>CINEMA</p>
            </div>
          </li>
          <div className="flex flex-col-reverse">
            {renderHistory()}

          </div>
        </ul>
      </div>
    </div>
  );
}
