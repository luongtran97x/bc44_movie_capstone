
import React from "react";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";

import ListMovieTablet from "./ListMovieTablet";
import ListMovieMobile from "./ListMovieMobile";
import ListMovieDesktop from "./ListMovieDesktop";

export default function ListMovie() {

  return (
    <>
      <Desktop>
        <ListMovieDesktop />
      </Desktop>

      <Tablet>
        <ListMovieTablet />
      </Tablet>

      <Mobile>
        <ListMovieMobile />
      </Mobile>
    </>
  );
}
