
import React from "react";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import TabsMovieTablet from "./TabsMovieTablet";
import TabMovieDesktop from "./TabsMovieDesktop";
import TabsMovieMoblie from "./TabsMovieMobile";

export default function TabsMovie() {


  return (

    <>
    <Desktop>
      <TabMovieDesktop/>
    </Desktop>
    <Tablet>
      <TabsMovieTablet/>
    </Tablet>
    <Mobile>
      <TabsMovieMoblie/>
    </Mobile>
    </>
  );
}
