import React from "react";

import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import SelectFlimMobile from "./SelectFlimMobile";
import SelectFlimDesktop from "./SelectFlimDesktop";
import SelectFlimTablet from "./SelectFlimTablet";

export default function SelectFlim() {
  return (
    <>
      <Desktop>
        <SelectFlimDesktop />
      </Desktop>
      <Tablet>
        <SelectFlimTablet />
      </Tablet>
      <Mobile>
        <SelectFlimMobile />
      </Mobile>
    </>
  );
}
