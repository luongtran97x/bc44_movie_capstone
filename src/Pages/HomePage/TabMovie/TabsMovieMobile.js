import { Tabs } from "antd";
import moment from "moment";
import React, { useState } from "react";
import { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { https } from "../../../Services/config";
import { Mobile } from "../../../responsive/responsive";

export default function TabsMovieMoblie() {
  const [heThongRap, setHeThongRap] = useState([]);
  useEffect(() => {
    https
      .get("api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP08")
      .then((res) => {
        setHeThongRap(res.data.content);
      })
      .catch((err) => {});
  }, []);

  let renderDsPhim = (dsp) => {
    return dsp.map((phim, index) => {
      return (
        <div key={index} className=" space-x-2">
          <div class="flex py-2 justify-center">
            <img
              src={phim.hinhAnh}
              alt="hinhAnh"
              style={{height:"200px"}}
              className="h-full w-40 object-cover rounded"
            />
          </div>
          <div className="grid grid-cols-2  w-full px-3 gap-2">
            {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu) => {
              return (
                <button  className="bg-orange-600 flex items-center hover:bg-orange-400  px-2 text-center transition duration-500 text-white rounded h-10 ">
                  <NavLink to={`/booking/${lichChieu.maLichChieu}`}>
                    {moment(lichChieu.ngayChieuGioChieu).format(
                      "DD/MM/YYYY ~ hh:mm"
                    )}
                  </NavLink>
                </button>
              );
            })}
          </div>
        </div>
      );
    });
  };

  let renderHeThongRap = () => {
    return heThongRap.map((heThong, index) => {
      return {
        key: index,
        label: <img src={heThong.logo} style={{width:"100px", maxWidth:"160%"}} alt="logo" />,
        children: (
          <Tabs
            tabPosition="top"
            defaultActiveKey="1"
            items={heThong.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.maCumRap,
                label: (
                  <div className="text-left  whitespace-normal">
                    <p className="text-orange-600 text-xl font-bold">
                      {cumRap.tenCumRap}
                    </p>
                    <p className="truncate  text-md flex-nowrap">
                      {cumRap.diaChi}
                    </p>
                  </div>
                ),
                children: renderDsPhim(cumRap.danhSachPhim),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div>
      <Mobile>
        
        <div className="py-5 pt-20" style={{ background: "#020d18" }}>
          <div className="container tabs rounded p-10  bg-white">
            <p className="pb-10 text-orange-600 font-bold xl:text-5xl md:text-4xl sm:text-3xl italic">
              NOW SHOWING
            </p>
            <Tabs
              tabPosition="top"
              defaultActiveKey="1"
              items={renderHeThongRap()}
            ></Tabs>
          </div>
        </div>
      </Mobile>
    </div>
  );
}
