import React from "react";

import "./User.css";
import UserDesktop from "./UserDesktop";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import UserTablet from "./UserTablet";
import UserMobile from "./UserMobile";
export default function UserInfo() {
  return (
    <>
      <Desktop>
        <UserDesktop />
      </Desktop>

      <Tablet>
        <UserTablet />
      </Tablet>

      <Mobile>
        <UserMobile />
      </Mobile>
    </>
  );
}
