import React from "react";
import "./History.css";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import HistoryDesktop from "./HistoryDesktop";
import HistoryTablet from "./HistoryTablet";
import HistoryMobile from "./HistoryMobile";
export default function HistoryBooking() {
  return (
    <>
      <Desktop>
        <HistoryDesktop />
      </Desktop>

      <Tablet>
        <HistoryTablet />
      </Tablet>

      <Mobile>
        <HistoryMobile />
      </Mobile>
    </>
  );
}
