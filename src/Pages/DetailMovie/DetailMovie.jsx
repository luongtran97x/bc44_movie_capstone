import React from "react";

import "./Detail.css";

import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import DetailMovieDesktop from "./DetailDesktop";
import DetailMovieMobile from "./DetailMobile";
import DetailMovieTablet from "./DetailTablet";
export default function DetailMovie() {
  return (
    <>
      <Desktop>
        <DetailMovieDesktop />
      </Desktop>

      <Tablet>
        <DetailMovieTablet />
      </Tablet>

      <Mobile>
        <DetailMovieMobile />
      </Mobile>
    </>
  );
}
