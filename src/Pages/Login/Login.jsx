import React from "react";

import "./Login.css"
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import LoginDesktop from "./LoginDesktop";
import LoginTablet from "./LoginTablet";
import LoginMobile from "./LoginMoblie";
export default function Login() {
 

  return (
    <>

    <Desktop>
      <LoginDesktop/>
    </Desktop>
    
    <Tablet>
      <LoginTablet/>
    </Tablet>

    <Mobile>
      <LoginMobile/>
    </Mobile>



    </>
  );
}
