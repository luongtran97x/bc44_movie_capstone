import React from "react";
import "./Booking.css";
import { Mobile, Tablet, Desktop } from "../../responsive/responsive";
import BookingDesktop from "./BookingDesktop";
import BookingTablet from "./BookingTablet";
import BookingMobile from "./DesktopMobile";
export default function BookingMovie() {
  return (
    <>
      <Desktop>
        <BookingDesktop />
      </Desktop>

      <Tablet>
        <BookingTablet />
      </Tablet>

      <Mobile>
        <BookingMobile />
      </Mobile>
    </>
  );
}
